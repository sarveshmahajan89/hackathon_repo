var performAccel = angular.module('performAccel', [
'ngRoute', 'performAccelIndex' , 'ui.bootstrap'
]);


performAccel.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/login', {
        templateUrl: '/login.html',
        controller: 'loginCtrl'
      }).
      when('/home', {
        templateUrl: 'partials/home.html',
        controller: 'homeCtrl'
      }).
      when('/faq', {
        templateUrl: 'partials/faq.html',
        controller: 'faqCtrl'
      }).
      when('/help', {
        templateUrl: 'partials/help.html',
        controller: 'helpCtrl'
      }).
      when('/settings', {
        templateUrl: 'partials/settings.html',
        controller: 'settingsCtrl'
      }).
      when('/reports', {
        templateUrl: 'partials/reports.html',
        controller: 'reportsCtrl'
      }).
      otherwise({
        redirectTo: '/home'
      });
  }]);